#ifndef UXDUMP_H
#define UXDUMP_H

#include <QMainWindow>

namespace Ui {
class UXDump;
}

class UXDump : public QMainWindow
{
    Q_OBJECT

public:
    explicit UXDump(QWidget *parent = 0);
    ~UXDump();

private:
    Ui::UXDump *ui;
};

#endif // UXDUMP_H
