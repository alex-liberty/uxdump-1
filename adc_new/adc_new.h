#ifndef ADC_NEW_H
#define ADC_NEW_H

#include <QSerialPort>
#include <QtSerialPort/QtSerialPort>
#include <QSerialPortInfo>
#include <QMainWindow>
#include <QString>
#include <QByteArray>
#include "qcustomplot.h"
#include <QMainWindow>
#include <QMessageBox>
#include <QJsonDocument>
#include <QJsonObject>
#include <QSocketNotifier>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

namespace Ui {
class ADC_new;
}

class ADC_new : public QMainWindow
{
    Q_OBJECT

public:
    explicit ADC_new(QWidget *parent = 0);
    QByteArray serialData;
    int num;
    bool plotting = false;
    bool log = false;

    ~ADC_new();
    static void termSignalHandler(int unused);


public slots:
    void handleSigTerm();
private slots:
    void CreateGUI();
//    void EnableControls(bool enable);
    void SetupPlot();

    void on_Button_Connect_clicked();


    void on_Button_Refresh_clicked();

    void clearControls();
    void readSerial();
    void plot();
    void replot();
    //////////////////////
    void update_LCD_NUM(QString sensor_reading);
    ///////////////////////
    void clear_LCD();
    void fill_LCD();
    void on_Button_Start_Log_clicked();

    void on_Button_Abort_clicked();
    void logging();


  //  void on_pushButton_clicked();

    void on_Button_Stop_Log_clicked();

protected:
    void closeEvent(QCloseEvent *event);
    void readConfig();
    void writeConfig();

private:
    Ui::ADC_new *ui;
    QSerialPort *UXDump;
    QTimer dataTimer;
    bool connected = false;
    //bool plotting = false;
    void enableControls(bool enable);
    int NUMBER_OF_POINTS = 3500;
    int dataPointNumber = 0;
    int state;
    QString receivedData;
    QByteArray GSR_str;
    QByteArray HR1_str;
    QByteArray HR2_str;
    QByteArray PTT_str;
    int GSR = 0;
    int HR1 = 0;
    int HR2 = 0;
    int PTT = 0;
    int BPM = 0;
    int BPM_2 = 0;
    int log_count = 0;
    QString LOGNAME = "";
    QDateTime last_timestamp;
    int accumulated_GSR, accumulated_BPM, accumulated_BPM_2, accumulated_PTT, number_of_accumulated_values;
    QSocketNotifier *snTerm;
    bool auto_mode = false;
    QString portName;

};

static int sigtermFd[2];
#endif // ADC_NEW_H
