#include <QCoreApplication>
#include <QProcess>
#include <signal.h>

QProcess process;

void termSignalHandler(int unused)
{   process.close();
    exit(0);
}
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QString cmd;
    cmd = "xterm -e ./Modules/EEGeventsDetectorFiles/EEGeventsDetector.sh";
    process.start(cmd);
    //process.waitForFinished();
    //process.close();

    struct sigaction term;
    term.sa_handler = termSignalHandler;
    sigemptyset(&term.sa_mask);
    term.sa_flags |= SA_RESTART;
    if (sigaction(SIGTERM, &term, 0) > 0) exit(-1);

    return a.exec();
}

