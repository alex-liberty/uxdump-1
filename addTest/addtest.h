#ifndef ADDTEST_H
#define ADDTEST_H

#include <QMainWindow>
#include <QProcess>
#include <QDir>
#include <QMessageBox>
#include <QDebug>
#include <QDate>
#include <QThread>


#include "databasework.h"
#include "loadercsv.h"

namespace Ui {
class AddTest;
}

class AddTest : public QMainWindow
{
    Q_OBJECT

public:
    explicit AddTest(QWidget *parent = 0);
    ~AddTest();

private slots:
    void on_processFinished();

    void on_pushButtonNext1_clicked();

    void on_pushButtonNext2_clicked();

    void on_pushButtonBack2_clicked();

    void on_pushButtonBack3_clicked();

    void onSettingsClicked();

    void onHelpClicked();

    void onHelpTestClicked();

    void on_pushButtonRun_clicked();

    void on_comboBoxLogin_currentTextChanged( const QString &arg1 );


    void on_treeWidgetModule_clicked(const QModelIndex &index);

    void on_treeWidgetTest_clicked(const QModelIndex &index);

private:
    void createFirstWindow();

    void createSecondWindow();

    void createThirdWindow();

    void createModuleList();

    void createTestList();

    void createTreeModule( QString moduleName, int countModule );

    void createTreeTest( QString testName, int countTest );

    void createSoftwareList();

    void createLoginList();

    void createDir();

    void readData();

    void runSelected();

    void createLog(QString moduleName);

    void parserData( QString moduleName );

    Ui::AddTest *ui;

    QString slash;
    DataBaseWork *db;
    LoaderCSV* csv;
    QProcess *confOpen;
    QProcess *proc;
    QList <QProcess*> processes;

    QString path;
    QStringList listModules;
    QStringList testList;
    QStringList softwareList;
    QStringList personLogins;
    QStringList logs;

    QDate *date;
    QString test;
    QStringList measuringModules;
    QString software;
    QString personLogin;
    QString sex;
    int YOB;
    QString note;
    QString dateTime;
    int duration;

    int logId;

    QString logDirectoryAfterAdd;
    QDir moduleDirectory;
    QDir softwareDirectory;
    QDir logDirectory;
    bool isExsist;

    QTime timeStart;
    QTime timeFinish;
    QProcess *testStart;
};

#endif // ADDTEST_H
