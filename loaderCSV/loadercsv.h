#ifndef LOADERCSV_H
#define LOADERCSV_H

#include "loadercsv_global.h"
#include "databasework.h"
#include <QStandardItemModel>
#include <QFile>
#include <QTextStream>

/**
 * @brief The LoaderCSV class
 * This class read log-file in CSV format and add notes in tables "Parameter" and "Value", using DataBaseWork library.
 */
class LOADERCSVSHARED_EXPORT LoaderCSV
{

public:
    LoaderCSV();
    ~LoaderCSV();
    void checkStringValue( QString &temp, int column, int idLog );
    void checkStringParameter(QString &temp);
    void CSVReader( int idLog, QString fileName );

     QFile *insertFile;

private:
    DataBaseWork* db;
    bool isParameter;
    QStringList Parameters;
    int column = 0;
    QChar apostrof = QChar(0x2BC);
    QStringList idParameters;
    int id;

    //QTextStream *insert;
};

#endif // LOADERCSV_H
