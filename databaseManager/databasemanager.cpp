#include "databasemanager.h"
#include "ui_databasemanager.h"


DatabaseManager::DatabaseManager(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::DatabaseManager)
{
    ui->setupUi( this );
    db = new DataBaseWork();
    db->connectBaseMySQL();
    writeDataToForm();
}

DatabaseManager::~DatabaseManager()
{
    delete ui;
}

void DatabaseManager::writeDataToForm()
{
    ui->comboBoxMeasuringModule->clear();
    ui->comboBoxTest->clear();
    ui->comboBoxTest->clear();
    ui->comboBoxLogin->clear();

    measuringModules = db->selectMeasuringModuleName();
    tests = db->selectTestName();
    testName = ui->comboBoxTest->currentText();
    softwares = db->selectSoftwareName( testName );
    logins = db->selectPersonLogin();

    ui->comboBoxMeasuringModule->addItems( measuringModules );
    ui->comboBoxTest->addItems( tests );
    ui->comboBoxLogin->addItems( logins );

    ui->comboBoxMeasuringModule_2->addItems( measuringModules );
    ui->comboBoxTest_2->addItems( tests );
    ui->comboBoxLogin_2->addItems( logins );
}

void DatabaseManager::on_comboBoxTest_currentTextChanged(const QString &arg1)
{
    softwares = db->selectSoftwareName( arg1 );
    ui->comboBoxSoftware->clear();

    if ( softwares.count() == 0 )
        softwares = db->selectSoftwareName();

    ui->comboBoxSoftware->addItems( softwares );
}

int DatabaseManager::readDataFromFormLoad()
{
    date = new QDate();
    int currentYear = date->currentDate().year();
    int lowYear = currentYear - 128;

    measuringModuleName = ui->comboBoxMeasuringModule->currentText();
    testName = ui->comboBoxTest->currentText();
    softwareName = ui->comboBoxSoftware->currentText();
    sex = ui->comboBoxSex->currentText();
    note = ui->lineEditNote->text();
    personLogin = ui->comboBoxLogin->currentText();
    YOB = ui->lineEditYOB->text().toInt();

    if ( measuringModuleName == "" ) {
        QMessageBox::warning( this, "Achtung!",
                              "Füllen Sie das Formular <<Measuring module>>, bitte" );
        return 0;
    }

    if ( testName == "" ) {
        QMessageBox::warning( this, "Achtung!",
                              "Füllen Sie das Formular <<Test>>, bitte" );
        return 0;
    }

    if ( softwareName == "" ) {
        QMessageBox::warning( this, "Achtung!",
                              "Füllen Sie das Formular <<Software>>, bitte" );
        return 0;
    }

    if ( personLogin == "" ) {
        QMessageBox::warning( this, "Achtung!",
                              "Füllen Sie das Formular <<Login>>, bitte" );
        return 0;
    }

    if ( YOB < lowYear || YOB > currentYear ) {
        QString error = QString( "Некорректное заполнение поля YEAR OF BIRTH. Год должен быть в диапазоне от %1 до %2" ).
                arg( lowYear ).arg( currentYear );
        QMessageBox::warning( this, "Achtung!", error );
        return 0;
    }

    return 1;
}

void DatabaseManager::on_pushButtonLoad_clicked()
{
    if ( !readDataFromFormLoad() )
        return;

    int duration = 0;
    QString dateTime = QDateTime::currentDateTime().toString( "dd/MM/yyyy hh:mm:ss:zzz" );
    QString fileName = QFileDialog::getOpenFileName ( this, "Open CSV file",
                                                      QDir::currentPath(), "CSV (*.csv)" );

    if ( fileName == "" )
        return;


    personLogin.replace("'", apostrof);
    testName.replace("'", apostrof);
    softwareName.replace("'", apostrof);
    measuringModuleName.replace("'", apostrof);
    note.replace("'", apostrof);

    personLogin.replace("\"", apostrof2);
    testName.replace("\"", apostrof2);
    softwareName.replace("\"", apostrof2);
    measuringModuleName.replace("\"", apostrof2);
    note.replace("\"", apostrof2);

    logId = db->addLog( personLogin, sex, YOB, testName, softwareName, measuringModuleName, dateTime, duration, note );

    csv = new LoaderCSV();
    csv->CSVReader( logId, fileName );
    this->close();
}

void DatabaseManager::on_comboBoxLogin_currentTextChanged(const QString &arg1)
{
    ui->comboBoxSex->setEnabled( true );
    ui->lineEditYOB->setEnabled( true );
    ui->lineEditYOB->setText( "" );

    sex = db->selectPersonSex( arg1 );
    if ( sex != "" ) {
        ui->comboBoxSex->setCurrentText( sex );
        ui->comboBoxSex->setEnabled( false );
    }

    QString YOBStr = db->selectPersonYOB( arg1 );
    if ( YOBStr != "" ) {
        ui->lineEditYOB->setText( YOBStr );
        ui->lineEditYOB->setEnabled( false );
    }
}

void DatabaseManager::on_pushButtonDelete_clicked()
{
    readDataFromFormDelete();
    QString param = measuringModuleName + ", " + testName + ", " + softwareName + ", " + personLogin;
    QMessageBox* pmbx =  new QMessageBox( "Achtung!",
                                          "<i>Achtung:</i>  <u>Entfernen der Einschreibung mit den Parametern:" + param +
                                          ". Bestätigen?</u>",
                                          QMessageBox::Information,
                                          QMessageBox::Yes,
                                          QMessageBox::No,
                                          QMessageBox::Cancel | QMessageBox::Escape );

    int n = pmbx->exec();
    delete pmbx;

    if ( n == QMessageBox::Yes )
        deleteItem();

    this->close();
}

void DatabaseManager::deleteItem()
{
    QList <int> listDelete;
    listDelete = db->selectLog( measuringModuleName, testName, softwareName, personLogin );

    if ( listDelete.count() == 0)
        QMessageBox::warning( this, "Error",
                              "We don't have item with these parameters" );

    if ( listDelete.count() == 1 ) {
        logId = listDelete[ 0 ];
        result = db->deleteLog( logId );
    }

    if( listDelete.count() > 1 )
        foreach (int id, listDelete)
            result = db->deleteLog( id );

    writeDataToForm();
}


void DatabaseManager::on_comboBoxLogin_2_currentTextChanged(const QString &arg1)
{
    measuringModuleName = ui->comboBoxMeasuringModule_2->currentText();
    testName = ui->comboBoxTest_2->currentText();
    softwareName = ui->comboBoxSoftware_2->currentText();
    personLogin = ui->comboBoxLogin_2->currentText();

    ui->comboBoxDateTime->clear();
    ui->comboBoxSex_2->setEnabled( true );
    ui->lineEditYOB_2->setEnabled( true );
    ui->lineEditYOB_2->setText( "" );

    sex = db->selectPersonSex( arg1 );
    QString YOBStr = db->selectPersonYOB( arg1 );

    if ( sex != "" ) {
        ui->comboBoxSex_2->setCurrentText( sex );
        ui->comboBoxSex_2->setEnabled( false );
    }

    if ( YOBStr != "" ) {
        ui->lineEditYOB_2->setText( YOBStr );
        ui->lineEditYOB_2->setEnabled( false );
    }

    idLogs = db->selectLog( measuringModuleName, testName, softwareName, personLogin );

    foreach (int idLog, idLogs)
        dateTimes.append( db->selectDateTime( idLog ) );

    ui->comboBoxDateTime->addItems( dateTimes );
}

int DatabaseManager::readDataFromFormDelete()
{
    date = new QDate();
    int currentYear = date->currentDate().year();
    int lowYear = currentYear - 128;

    YOB = ui->lineEditYOB_2->text().toInt();

    if ( YOB < lowYear || YOB > currentYear ) {
        QString error = QString( "Некорректное заполнение поля YEAR OF BIRTH. Год должен быть в диапазоне от %1 до %2" ).
                arg( lowYear ).arg( currentYear );

        QMessageBox::warning( this, "Achtung!", error );
        return 0;
    }

    measuringModuleName = ui->comboBoxMeasuringModule_2->currentText();
    testName = ui->comboBoxTest_2->currentText();
    softwareName = ui->comboBoxSoftware_2->currentText();
    personLogin = ui->comboBoxLogin_2->currentText();
    sex = ui->comboBoxSex_2->currentText();
    return 1;
}

void DatabaseManager::on_comboBoxTest_2_currentTextChanged(const QString &arg1)
{
    softwares = db->selectSoftwareName( arg1 );
    ui->comboBoxSoftware_2->clear();

    if ( softwares.count() == 0 )
        softwares = db->selectSoftwareName();

    ui->comboBoxSoftware_2->addItems( softwares );
}
